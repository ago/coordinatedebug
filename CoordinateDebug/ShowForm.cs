﻿// Copyright(c) 2018 Axel Gembe<derago@gmail.com>
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

using System.Windows.Forms;

namespace CoordinateDebug
{
    public partial class ShowForm : Form
    {
        public ShowForm()
        {
            InitializeComponent();

            Icon = Properties.Resources.CoordinateDebug;
        }

        private void ShowForm_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 27) // ESC
                Close();
        }
    }
}
