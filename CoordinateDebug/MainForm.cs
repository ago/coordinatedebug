﻿// Copyright(c) 2016, 2018 Axel Gembe<derago@gmail.com>
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;

namespace CoordinateDebug
{
    public partial class MainForm : Form
    {
        private Color _color;

        public Color Color
        {
            get { return _color; }

            set {
                if (value == _color)
                    return;

                _color = value;

                colorTextBox.Text = _color.ToString();
            }
        }

        public MainForm()
        {
            InitializeComponent();

            Icon = Properties.Resources.CoordinateDebug;

            Color = Color.Green;

            widthTextBox.Text = SystemInformation.VirtualScreen.Width.ToString();
            heightTextBox.Text = SystemInformation.VirtualScreen.Height.ToString();
        }

        private void GenerateFile()
        {
            var sfd = new SaveFileDialog();
            sfd.ShowDialog(this);

            var bmp = DrawBitmap();
            bmp.Save(sfd.FileName, ImageFormat.Png);
        }

        private Bitmap DrawBitmap()
        {
            var size = new Size(int.Parse(widthTextBox.Text), int.Parse(heightTextBox.Text));
            var step = new Size(int.Parse(stepXTextBox.Text), int.Parse(stepYTextBox.Text));

            var bmp = new Bitmap(size.Width, size.Height);

            Redraw(bmp, Color, step);

            return bmp;
        }


        private void DrawEllipse(Graphics g, Pen pen, float x, float y, float rx, float ry)
        {
            g.DrawEllipse(pen, x - rx, y - ry, rx * 2.0f, ry * 2.0f);
        }

        private void Redraw(Bitmap bmp, Color bgColor, Size step)
        {
            float cx = bmp.Width / 2.0f;
            float cy = bmp.Height / 2.0f;
            float aspect = bmp.Width / (float)bmp.Height;

            using (var g = Graphics.FromImage(bmp)) {
                g.FillRectangle(new SolidBrush(bgColor), 0, 0, bmp.Width, bmp.Height);

                float rx = bmp.Width;
                float ry = bmp.Height;
                while (rx > 0.0f && ry > 0.0f) {
                    DrawEllipse(g, Pens.LightGreen, cx, cy, rx, ry);
                    rx -= 24.0f * aspect;
                    ry -= 24.0f;
                }

                for (var y = 0; y < bmp.Height; y += step.Height) {
                    for (var x = 0; x < bmp.Width; x += step.Width) {
                        g.DrawString(string.Format("{0}x{1}", x, y), SystemFonts.DefaultFont, Brushes.Black, x + 1, y + 1);
                        g.DrawString(string.Format("{0}x{1}", x / step.Width, y / step.Height), SystemFonts.DefaultFont, Brushes.Black, x + 1, y + 22);
                        g.DrawRectangle(Pens.Black, x, y, step.Width, step.Height);
                    }
                }
            }
        }

        private void generateButton_Click(object sender, EventArgs e)
        {
            GenerateFile();
        }

        private void pickButton_Click(object sender, EventArgs e)
        {
            var cd = new ColorDialog();
            cd.Color = Color;

            if (cd.ShowDialog() == DialogResult.OK)
                Color = cd.Color;
        }

        private void showButton_Click(object sender, EventArgs e)
        {
            var sf = new ShowForm {
                StartPosition = FormStartPosition.Manual,
                Location = new Point(0, 0),
                Size = SystemInformation.VirtualScreen.Size
            };

            sf.PictureBox.Image = DrawBitmap();

            sf.ShowDialog();
        }
    }
}
